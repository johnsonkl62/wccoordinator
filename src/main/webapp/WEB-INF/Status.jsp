<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Employer Collection Cache</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <head>
            <%@ page isELIgnored="false" %>
        </head>
    </head>
    <body>


<table class="table-bordered myblue table-hover" style="width: 1175px;">
<thead>
	  <tr>
       <th style="width: 60px;">Updated</th>
       <th style="width: 60px;">IP</th>
       <th style="width: 60px;">FN</th>
       <th style="width: 60px;">County</th>
       <th style="width: 60px;">Carrier</th>
       <th style="width: 60px;">Employer Name</th>
       <th style="width: 60px;">Address</th>
       <th style="width: 60px;">City</th>
       <th style="width: 60px;">State</th>
       <th style="width: 60px;">Zip</th>
       <th style="width: 60px;">Xdate</th>
       <th style="width: 60px;">GCC</th>
       <th style="width: 60px;">Mod/Merit</th>
	  </tr>
</thead>
<tbody>


     <c:forEach items="${employers}" var="employer">
         <tr>
	       <td>&nbsp;</td>
	       <td>${employer.county}</td>
	       <td>${employer.carrier}</td>
	       <td>${employer.name}</td>
	       <td>${employer.addr}</td>
	       <td>${employer.city}</td>
	       <td>${employer.state}</td>
	       <td>${employer.zip}</td>
           <td><fmt:formatDate pattern = "yyyy-MM-dd" value = "${employer.xdate}" /></td>
	       <td>${employer.code}</td>
	       <td>${employer.mm}</td>
         </tr>
    </c:forEach>   		
	
    </tbody>
    </table>
    </body>
</html>