package com.synesis.service.policies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.policies.PoliciesDao;
import com.synesis.dto.policy.EmployerDetailsDto;

@Service
public class PoliciesService {

	@Autowired
	private PoliciesDao policiesDao;
	

	@Transactional
	public void updatePolicies(EmployerDetailsDto dto) {

		policiesDao.updatePolicies(dto);
	}


}
