package com.synesis.service.mysql;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.mysql.MysqlDao;

@Service
public class MysqlService {

	@Autowired
	private MysqlDao mysqlDao;
	
//	@Override
	@Transactional(readOnly=true)
	public HashMap<String, String> getStatus() {

		return mysqlDao.getStatus();
	}

}
