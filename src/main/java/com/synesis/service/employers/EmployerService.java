package com.synesis.service.employers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.employers.EmployerDao;
import com.synesis.model.Employer;
import com.synesis.utilities.CollectionStatus;

@Service
public class EmployerService {

	@Autowired
	private EmployerDao employerDao;
	
//	@Override
	@Transactional(readOnly=true)
	public List<Employer> getEmployers() {

		return employerDao.getEmployers();
	}

	
//	// // @Override
//	@Transactional(readOnly=true)
//	public Employer getEmployer(List<String> counties, int daysOld) {
//		
//		return employerDao.getEmployer(counties, daysOld);
//	}
//
//
//	@Transactional(readOnly=true)
//	public Employer getOldestEmployer() {
//		
//		return employerDao.getEmployer();
//	}


	// // @Override
	@Transactional
	public void updateEmployer(Employer employer) {

		employerDao.updateEmployer(employer);
	}



	// // @Override
	@Transactional(readOnly=true)
	public Employer findOne(String employerId) {
		
		return employerDao.findOne(employerId);
	}
	
	
	// // @Override
	@Transactional(readOnly=true)
	public List<String> getPriorityCounties(int daysOld) {
		return employerDao.getPriorityCounties(daysOld);
	}


	public CollectionStatus getCurrentStatus() {
		return employerDao.getCurrentStatus();
	}

}
