package com.synesis.dao.employers;
//
//import java.util.List;
//
//import com.synesis.model.Employer;
//
//
//public interface EmployerDao {
//
//	public Employer getEmployer(List<String> counties, int daysOld);
//
//	public List<Employer> getEmployers(List<String> counties, int daysOld);
//
//	public List<String> getPriorityCounties(int daysOld);
//
//	public void updateEmployer(String employerId, String code);
//
//	public Employer findOne(String employerId);
//	
//}


import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Employer;
import com.synesis.utilities.CollectionStatus;

@Repository
public class EmployerDao {

	
	@Autowired
	private SessionFactory sessionFactory;

	
	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	
	@SuppressWarnings("unchecked")
	public List<Employer> getEmployers() {
		List<Employer> employers = new ArrayList<Employer>();
		List<Employer> additional = null;
		try {
//			try {
//				employers = (List<Employer>) sessionFactory.getCurrentSession()
//						.createQuery(
//								"FROM Employer e WHERE e.county in ('LAWRENCE', 'MERCER', 'CRAWFORD', 'VENANGO', 'BEAVER', 'BUTLER', 'WASHINGTON', 'WARREN', 'FOREST', 'CLARION', 'ARMSTRONG', 'WESTMORELAND', 'JEFFERSON', 'INDIANA') "
//										+ " AND e.updated is null ORDER BY RAND()")
//						.setMaxResults(20000).getResultList();
//			} catch (Exception e) {
//				employers = null;
//				e.printStackTrace();
//			}
//
//			if (employers == null || employers.size() == 0) {
//				try {
//					employers = (List<Employer>) sessionFactory.getCurrentSession()
//							.createQuery(
//									"FROM Employer e WHERE (e.county is not null AND e.county!='PHILADELPHIA' AND e.county!='DELAWARE' AND e.county!='EXAMPLE') "
//											+ " AND e.updated is null ORDER BY RAND()")
//							.setMaxResults(20000).getResultList();
//				} catch (Exception e) {
//					employers = null;
//					e.printStackTrace();
//				}
//
//				if (employers == null || employers.size() == 0) {

			try {
				employers = (List<Employer>) sessionFactory.getCurrentSession()
						.createQuery("FROM Employer e WHERE (e.county is not null AND e.county!='EXAMPLE') "
								+ " AND e.updated is null ORDER BY RAND()")
						.setMaxResults(20000).getResultList();
			} catch (Exception e) {
				employers = null;
				e.printStackTrace();
			}
			//
//					if (employers == null || employers.size() == 0) {


			if ( employers.size() < 5 ) {
			
			int ageInMonths = 12;  // Reduced each iteration
				while (employers.size() < 2500) {
					try {
						LocalDate ld = LocalDate.now().minusMonths(ageInMonths--);
						additional = (List<Employer>) sessionFactory.getCurrentSession()
								.createQuery("FROM Employer e WHERE (e.county is not null AND e.county!='EXAMPLE') "
										+ " AND e.updated < '" + ld.format(dtf) + "' ORDER BY RAND()")
								.setMaxResults(20000).getResultList();
						employers.addAll(additional);
					} catch (Exception e) {
						employers = null;
						e.printStackTrace();
					}
				}
			}

//				}
//			}
			return employers;
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	public List<String> getPriorityCounties(int age) {
		LocalDateTime ageLimit = LocalDateTime.now().minusDays(age);
		List<String> counties = sessionFactory.getCurrentSession().createQuery("SELECT DISTINCT(e.county) FROM Employer e WHERE e.county is not null AND (e.updated IS NULL OR e.updated<'" + ageLimit.format(dtf) + "')").getResultList();
		return counties;
	}

	
	
	public void updateEmployer(Employer emp) throws NullPointerException {
		
		try {
			Employer employer = (Employer) sessionFactory.getCurrentSession().createQuery("from Employer e WHERE e.fn=:empId ")
							.setParameter("empId", emp.getFn())
							.getSingleResult();
			
			if(employer!=null) {
				employer.setAddr(emp.getAddr());
				employer.setCarrier(emp.getCarrier());
				employer.setCity(emp.getCity());
				employer.setCode(emp.getCode());
				employer.setCounty(emp.getCounty());
				employer.setMm(emp.getMm());
				employer.setName(emp.getName());
				employer.setState(emp.getState());
				setUpdatedField(employer);
				
				employer.setXdate(emp.getXdate());
				employer.setZip(emp.getZip());

				sessionFactory.getCurrentSession().update(employer);
				sessionFactory.getCurrentSession().flush();
			}
			
		} catch (javax.persistence.NoResultException e) {
			System.out.println("FN " + emp.getFn() + " not found in database.");
			throw e;
			
		} catch (NullPointerException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpdatedField(Employer employer) {
		LocalDateTime ldt = LocalDateTime.now();
		LocalDateTime eu = employer.getUpdated().toLocalDateTime();
		
		if (ldt.toLocalDate().isAfter(eu.toLocalDate())) {
			employer.setUpdated(Timestamp.valueOf(ldt));
		}
	}


	public Employer findOne(String employerId) {
	
		try {
			Employer employer = (Employer) sessionFactory.getCurrentSession().createQuery("from Employer e WHERE e.fn=:empId ")
							.setParameter("empId", employerId)
							.getSingleResult();
			
			return employer;
			
		} catch (Exception e) {
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	public CollectionStatus getCurrentStatus() {
		CollectionStatus cs = new CollectionStatus();
		try {
			List<Object[]> list = sessionFactory.getCurrentSession()
					.createNativeQuery("SELECT MONTH(xdate) AS x, COUNT(*) FROM employers WHERE updated IS NULL GROUP BY x ORDER BY x ").getResultList();
			for ( Object[] entry : list ) {
				cs.addNullCount((Integer)entry[0], ((BigInteger)entry[1]).intValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		try {
			List<Object[]> list = sessionFactory.getCurrentSession()
					.createNativeQuery("SELECT MONTH(updated) AS m, YEAR(updated) AS y, COUNT(*) FROM employers WHERE updated IS NOT NULL GROUP BY y,m ORDER BY y,m ").getResultList();
			for ( Object[] entry : list ) {
				cs.addUpdatedCount((Integer)entry[0], (Integer)entry[1], ((BigInteger)entry[2]).intValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		return cs;
	}

}
