package com.synesis.dao.mysql;
//
//import java.util.List;
//
//import com.synesis.model.Employer;
//
//
//public interface EmployerDao {
//
//	public Employer getEmployer(List<String> counties, int daysOld);
//
//	public List<Employer> getEmployers(List<String> counties, int daysOld);
//
//	public List<String> getPriorityCounties(int daysOld);
//
//	public void updateEmployer(String employerId, String code);
//
//	public Employer findOne(String employerId);
//	
//}


import java.util.HashMap;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MysqlDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public HashMap<String, String> getStatus() {
		HashMap<String, String> status = new HashMap<String, String>();
		try {
			try {
				List<Object[]> result = (List<Object[]>) sessionFactory.getCurrentSession().createNativeQuery("SHOW GLOBAL STATUS").getResultList();
				for (Object[] var : result) {
					status.put((String)var[0], (String)var[1]);
				}
			} catch (Exception e) {
				status = null;
				e.printStackTrace();
			}
			return status;
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return null;
	}

}
