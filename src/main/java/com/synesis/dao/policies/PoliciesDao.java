package com.synesis.dao.policies;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.dto.policy.PolicyDetails;
import com.synesis.model.Policy;

@Repository
public class PoliciesDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public void updatePolicies(EmployerDetailsDto dto) {
		Session session = sessionFactory.getCurrentSession();

		//
		//  Delete existing entries for this FN
		//
		try {
			List<PolicyDetails> policyList = session.createQuery("from Policy p WHERE p.fn=:empFn ")
							.setParameter("empFn", dto.getFileNumber())
							.getResultList();

			if ( policyList != null ) {
				for ( PolicyDetails pd : policyList ) {
					sessionFactory.getCurrentSession().delete(pd);
					sessionFactory.getCurrentSession().flush();
				}
			}
		} catch (NullPointerException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		//
		//  Enter new policy list from current retrieval
		//
		try {
			for (PolicyDetails pd : dto.getPolicyDetails()) {
				Policy p = new Policy(dto.getFileNumber(), pd.getLineNumber(), pd.getPolicyNumber(), pd.getInsuranceCarrier(), pd.getNaic(), pd.getEffectiveDate(), pd.getExpirationDate(), pd.getCancelDate());
				session.save(p);
			}
		} catch (NullPointerException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
