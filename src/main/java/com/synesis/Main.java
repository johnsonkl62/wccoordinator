package com.synesis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class Main {

	public static String externalIP = "Unassigned";
	
	public static void main(String[] args) {

		try {
			JSONObject response = vmStartup();
			Integer delayInSeconds = (Integer)response.get("delayInSeconds");
			externalIP = (String)response.get("ipAddress");
			while (delayInSeconds != null) {
				try {
					LocalDateTime targetWake = LocalDateTime.now().plusSeconds(delayInSeconds);
					System.out.println("Est wake: " + targetWake);
					Thread.sleep(delayInSeconds*1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				vmNextEmp();

				VMSummary();
				
				delayInSeconds = VMDetails();
				
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static JSONObject vmStartup() throws IOException,
			ClientProtocolException {
		String url = "http://localhost:8080/wccoordinator/VMStartup";

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		// add request header
		HttpResponse response = client.execute(request);

		if (response.getStatusLine().getStatusCode() == 200) {

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			if (result.toString() != null) {
				JSONObject jsonObject = new JSONObject(result.toString());
				System.out.println(jsonObject);
				return jsonObject;
			}
		}
		return null;
	}

	private static void vmNextEmp() throws IOException, ClientProtocolException {
		String url = "http://localhost:8080/wccoordinator/VMNextEmp";

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		// add request header
		HttpResponse response = client.execute(request);

		if (response.getStatusLine().getStatusCode() == 200) {

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			if (result.toString() != null) {

				JSONArray jsonArray = new JSONArray(result.toString());

				Iterator<Object> iterator = jsonArray.iterator();

				while (iterator.hasNext()) {
					JSONObject jsonObject = (JSONObject) iterator.next();
					System.out.println(jsonObject);
				}
			}
		}
	}

	
	private static void VMSummary() throws IOException, ClientProtocolException {
		String url = "http://localhost:8080/wccoordinator/VMSummary";

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		// add request header
		HttpResponse response = client.execute(post);

		if (response.getStatusLine().getStatusCode() == 200) {

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			if (result.toString() != null) {

				JSONObject jsonObject = new JSONObject(result.toString());
				System.out.println(jsonObject);

			}
		}

	}
	
	
	private static Integer VMDetails() throws IOException, ClientProtocolException {
		String url = "http://localhost:8080/wccoordinator/VMDetails";

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		Integer delayInSeconds = null;
		
		// add request header
		HttpResponse response = client.execute(post);

		if (response.getStatusLine().getStatusCode() == 200) {

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			if (result.toString() != null) {
				JSONObject jsonObject = new JSONObject(result.toString());
				System.out.println(jsonObject);
				delayInSeconds = jsonObject.getInt("delayInSeconds");
			}
		}
		return delayInSeconds;
	}
	
}
