package com.synesis.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.synesis.utilities.FormatUtils;

@Entity
@Table(name = "employers")
public class Employer  implements Serializable {

	private static final long serialVersionUID = 1L;
	private String fn;
	private String fein;    //   NEW FIELD (FEDERAL EMPLOYER ID NUMBER)
	private String code;
	private String mm;
	private Date xdate;
	private String name;
	private String pname = "";    //  NEW FIELD (PRIMARY NAME)
	private String addr;
	private String city;
	private String state;
	private String zip;
	private String county;
	private String carrier;
	private Timestamp updated;

	public Employer() {
	}

	@Id
	@Column(name = "fn", unique = true, nullable = false, length = 10)
	public String getFn() {
		return fn;
	}

	public void setFn(String fn) {
		this.fn = fn;
	}

	@Column(name = "fein")
	public String getFein() {
		return fein;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	@Column(name="code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
		if ( code == null || !isAlphaNumeric(code) ) { 
			this.code = "";
		}
		// Remove leading zeroes
		try {
			Integer i = Integer.parseInt(this.code);
			this.code = i.toString();
		} catch (Exception e) {
		}
	}

	@Column(name = "mm")
	public String getMm() {
		return mm;
	}

	public void setMm(String mm) {
		this.mm = mm;
	}

	@Column(name = "xdate")
	public Date getXdate() {
		return xdate;
	}

	public void setXdate(Date xdate) {
		this.xdate = xdate;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "pname")
	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	@Column(name = "addr")
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "zip")
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "county")
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Column(name = "carrier")
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	@Column(name = "updated")
	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	private boolean isAlphaNumeric(String code) {
		return code.matches("[a-zA-Z0-9]+");
	}

	@Override
	public String toString() {
		return  fn + "\t" + 
				county + "\t" + 
				updated + "\t" + 
				carrier + "\t" + 
				name + "\t" + 
				addr + "\t" + 
				city + "\t" + 
				state + "\t" + 
				zip + "\t" + 
				xdate + "\t" + 
				code + "\t" + 
				mm;
	}

	public String toFixedString() {
		return  FormatUtils.fixedWidth(fn,7) + "\t" + 
				FormatUtils.fixedWidth(county,14) + "\t" + 
				FormatUtils.dtsdf.format(updated) + "\t" + 
				FormatUtils.fixedWidth(carrier,40) + "\t" + 
				FormatUtils.fixedWidth(name,50) + "\t" + 
				FormatUtils.fixedWidth(addr,30) + "\t" + 
				FormatUtils.fixedWidth(city,30) + "\t" + 
				FormatUtils.fixedWidth(state,2) + "\t" + 
				FormatUtils.fixedWidth(zip,5) + "\t" + 
				FormatUtils.dsdf.format(xdate) + "\t" + 
				FormatUtils.fixedWidth(code,4) + "\t" + 
				mm;
	}
}
