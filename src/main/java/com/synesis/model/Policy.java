package com.synesis.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "policies")
public class Policy implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Basic
	@Column(name = "fn", nullable = false)
	private String fn;

	@Id
	@Basic
	@Column(name = "line", nullable = false)
	private Integer line;
	
	@Basic
	@Column(name = "polnum")
	private String policy_number;

	@Basic
	@Column(name = "carrier")
	private String carrier;

	@Basic
	@Column(name = "naic")
	private String naic;

	@Basic
	@Column(name = "effDate")
	private LocalDate effDate;

	@Basic
	@Column(name = "expDate")
	private LocalDate expDate;
	
	@Basic
	@Column(name = "canDate")
	private LocalDate canDate;

	public Policy() {
	}

	public Policy(String fn, Integer line, String policy_number, String carrier, String naic, LocalDate effDate, LocalDate expDate, LocalDate canDate) {
		this.fn = fn;
		this.line = line;
		this.policy_number = policy_number;
		this.carrier = carrier;
		this.naic = naic;
		this.effDate = effDate;
		this.expDate = expDate;
		this.canDate = canDate;
	}

	public Policy(String fn, Integer line, String policy_number, String carrier, String naic, String effDate, String expDate, String canDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");

		this.fn = fn;
		this.line = line;
		this.policy_number = policy_number;
		this.carrier = carrier;
		this.naic = naic;

		try {
			this.effDate = LocalDate.parse(effDate,formatter);
		} catch (Exception e) {
			this.effDate = null;
		}

		try {
			this.expDate = LocalDate.parse(expDate,formatter);
		} catch (Exception e) {
			this.expDate = null;
		}
		
		try {
			this.canDate = LocalDate.parse(canDate,formatter);
		} catch (Exception e) {
			this.canDate = null; 
		}
	}
	
	public String getFn() {
		return fn;
	}

	public void setFn(String fn) {
		this.fn = fn;
	}

	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public String getPolicy_number() {
		return policy_number;
	}

	public void setPolicy_number(String policy_number) {
		this.policy_number = policy_number;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getNaic() {
		return naic;
	}

	public void setNaic(String naic) {
		this.naic = naic;
	}

	public LocalDate getEffDate() {
		return effDate;
	}

	public void setEffDate(LocalDate effDate) {
		this.effDate = effDate;
	}

	public LocalDate getExpDate() {
		return expDate;
	}

	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}

	public LocalDate getCanDate() {
		return canDate;
	}

	public void setCanDate(LocalDate canDate) {
		this.canDate = canDate;
	}

	@Override
	public String toString() {
		return  fn + "\t" + 
				line + "\t" + 
				policy_number + "\t" + 
				carrier + "\t" + 
				naic + "\t" + 
				effDate + "\t" + 
				expDate + "\t" + 
				canDate;
	}
}
