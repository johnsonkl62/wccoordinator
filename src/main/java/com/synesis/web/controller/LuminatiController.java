package com.synesis.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.config.Folders;
import com.synesis.dto.policy.EmployerDetailsDto;
import com.synesis.model.Employer;
import com.synesis.service.employers.EmployerService;
import com.synesis.service.policies.PoliciesService;
import com.synesis.utilities.FormatUtils;
import com.synesis.utilities.GuassianDelays;
import com.synesis.utilities.SendMailUtility;

@Controller
public class LuminatiController {

	static private DateTimeFormatter lsdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd\tHH:mm:ss.SSS");
	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd\tHH:mm:ss");

	static private Integer synch = new Integer(0); 
	
	@Autowired
	private EmployerService employerService;
	
	@Autowired
	private PoliciesService policiesService;
	
    private static final String WEBSITE_IP = "207.244.242.208";   //  Contabo server

	static {
		java.util.logging.Logger.getLogger("*.*").setLevel(Level.SEVERE);
		
        try {
            String myIp = Executor.newInstance()
                    .execute(Request.Get("https://api64.ipify.org/"))
                    .returnContent().asString();

            if (!WEBSITE_IP.equals(myIp)) {
                Folders.BASE_DIRECTORY = Folders.WINDOWS_BASE_DIRECTORY;  
                Folders.COLLECTION_DIRECTORY = Folders.WINDOWS_COLLECTION_DIRECTORY;  
            }
    		
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	

	@RequestMapping(value = "Luminati", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getData(HttpServletRequest request) {
		String response = "Call failed ...";
		
		java.util.logging.Logger.getLogger("*.*").setLevel(Level.SEVERE);
		
		String option = request.getParameter("option");
		if ( option == null || "".equals(option) || "delay".equalsIgnoreCase(option) ) {
			response = processDelayRequest(request, response);
		} else if ( "reset".equalsIgnoreCase(option) ) {
			response = processResetRequest(request);
		} else if ( "employer".equalsIgnoreCase(option) ) {
			response = processEmployerRequest(request);
		}
		return response;
	}


	@RequestMapping(value = "Status", method = RequestMethod.GET)
	public ModelAndView getStatus(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		List<Employer> emps = null;
		int count = 50000;

		String option = request.getParameter("option");
		if ( option == null || "".equals(option) ) {
			emps = EmployerController.getEmployerCache();

		} else if ( "dump".equalsIgnoreCase(option) ) {
			count = 20;
			String countStr = request.getParameter("count");
			if ( countStr != null ) {
				try {
					count = Integer.parseInt(countStr);
				} catch ( Exception e ) {
					count = 25;
				}
			}
			emps = EmployerController.getPendingCache();
		}

		if(emps !=null && emps.size() > count) {
			emps = emps.subList(0, count-1);
		}

		model.addObject("employers", emps);
		model.setViewName("Status");
		return model;
	}
	

	@RequestMapping(value = "Save", method = RequestMethod.GET)
	public ModelAndView processSaveRequest(HttpServletRequest request) {
		return updateDatabase();
	}
	
//	@RequestMapping(value = { "/Status" }, method = { RequestMethod.GET })
//	public ModelAndView statusPage(HttpServletRequest request) {
//		ModelAndView model = new ModelAndView();
//		List<Employer> emps = EmployerController.getEmployerCache();
//		model.addObject("empCache", emps);

//   FAILS ...  JSP not interrupting JSTL 

//		model.setViewName("EmpCache");
//		return model;
//	}
	


	@RequestMapping(value = "Luminati", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postEmployer(HttpServletRequest request) {
		String response = "Call failed ...";

		String option = request.getParameter("option");
		if ( "policies".equalsIgnoreCase(option) ) {
			response = processPoliciesPost(request, response);
		} else { 
			response = processEmployerPost(request, response);
		}
		return response;
	}


	private String processResetRequest(HttpServletRequest request) {
		GuassianDelays gc = GuassianDelays.getInstance();
		return gc.reset();
	}

	private String processDelayRequest(HttpServletRequest request, String response) {
		String ip = getIp(request);

		DelayResponse delay = getDelayResponse(ip);
		logString(LocalDateTime.now().format(lsdtf) + "\tGET\tDelay\t" + delay.getDelay());
		try {
			response = new ObjectMapper().writeValueAsString(delay);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}


	private String getIp(HttpServletRequest request) {
		String ip = request.getRemoteAddr();
		if (ip == null) {
			ip = request.getHeader("X-FORWARDED-FOR");
			if (ip == null) {
				ip = request.getParameter("ip");
			}
		}
		return ip;
	}
	

	private DelayResponse getDelayResponse(String ip) {
		GuassianDelays gc = GuassianDelays.getInstance();
		DelayResponse dr = new DelayResponse();
		dr.setMyIp(ip);
		dr.setDelay(gc.getDelay());
		return dr;
	}


	private String processEmployerRequest(HttpServletRequest request) {
		String result = "{}";
		try {
			if ( EmployerController.remainingCache() < 5 ) {
				synchronized (synch) {
					if ( EmployerController.remainingCache() < 5 ) {
						List<Employer> elist = employerService.getEmployers();
						EmployerController.fillCache(elist);
						sendEmail(elist);
					}
				}
			}

			Employer emp = EmployerController.getEmployer();

			logString(LocalDateTime.now().format(lsdtf) + "\tGET\t\t\t" + 
					FormatUtils.fixedWidth(emp.getFn(),7) + "\t" + 
					(emp.getUpdated() == null ? "\t" : FormatUtils.dtsdf.format(emp.getUpdated())) + "\t" + 
					FormatUtils.fixedWidth(emp.getCounty(),14) + "\t" + 
					FormatUtils.fixedWidth(emp.getName(),50) + "\t" + 
					FormatUtils.fixedWidth(emp.getCarrier(),40) + "\t" + 
					(emp.getXdate() == null ? "\t\t" : FormatUtils.dsdf.format(emp.getXdate())) + "\t" + 
					FormatUtils.fixedWidth(emp.getCode(),4) + "\t" + 
					FormatUtils.fixedWidth(emp.getMm(),7));

			return new ObjectMapper().writeValueAsString(emp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	private void sendEmail(List<Employer> elist) {
		try {
			StringBuffer sb = new StringBuffer();
//			Collections.sort(elist, (Employer a, Employer b) -> a.getFn().compareTo(b.getFn()));
			for (Employer emp : elist) {
				sb.append(emp.toFixedString()+"\r\n");
			}
			LocalDateTime ldt = LocalDateTime.now();
			SendMailUtility.emailReport(sb.toString(), "Employer Cache filled at " + ldt.format(dtf) + " - " + elist.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private String processEmployerPost(HttpServletRequest request, String response) {
		
		StringBuilder result = new StringBuilder("Failure log:");

		ObjectMapper mapper = new ObjectMapper();
		String ip = getIp(request);  

		result.append("\nip = " + ip);

		try {
			String json = request.getParameter("data");
			result.append("\njson = " + json);

			Employer emp = mapper.readValue(json, Employer.class); 

			result.append("\nemp = " + emp.toFixedString());

			DelayResponse delay = getDelayResponse(ip);
		    try {
				LocalDateTime ldt = LocalDateTime.now();
				String delayStr = ( delay != null ? delay.getDelay().toString() : "null delay");

				result.append("\ndelayStr = " + delayStr);

				logString(ldt.format(lsdtf) + "\tPOST\t" + ip + "\t" + 
						FormatUtils.fixedWidth(emp.getFn(),7) + "\t" + 
						FormatUtils.dtsdf.format(emp.getUpdated()) + "\t" + 
						FormatUtils.fixedWidth(emp.getCounty(),14) + "\t" + 
						FormatUtils.fixedWidth(emp.getName(),50) + "\t" + 
						FormatUtils.fixedWidth(emp.getCarrier(),40) + "\t" + 
						FormatUtils.dsdf.format(emp.getXdate()) + "\t" + 
						FormatUtils.fixedWidth(emp.getCode(),4) + "\t" + 
						FormatUtils.fixedWidth(emp.getMm(),7) + "\tDelay\t" + delayStr);
//				System.out.println(LocalDateTime.now().format(lsdtf) + "\tGET\t\t\t" + emp.getFn() + "\t" + emp.getCounty() + "\t" + emp.getName() + "\t" + sdf.format(emp.getXdate()));

			} catch (Exception e) {
				result.append("\nError printing record");
				logString("Error printing record");
			}

	    	try {
	    		EmployerController.putEmployer(emp);
	    	} catch (Exception e ) {
	    		result.append("\nException in putEmployer ...\n" + stackTrace(e));
	    		e.printStackTrace();
	    	}

	    	String newResult = mapper.writeValueAsString(delay);
	    	result.setLength(0);
	    	result.append(newResult);
		} catch ( Exception e ) {
			e.printStackTrace();
			result.append("\nException ... \n" + stackTrace(e));
		}
		return result.toString();
	}


	private String processPoliciesPost(HttpServletRequest request, String response) {
		
		StringBuilder result = new StringBuilder("Failure log:");

		ObjectMapper mapper = new ObjectMapper();
		String ip = getIp(request);  

		result.append("\nip = " + ip);

		try {
			String json = request.getParameter("details");
			result.append("\njson = " + json);

			EmployerDetailsDto policies = mapper.readValue(json, EmployerDetailsDto.class); 

			result.append("\npolicies = " + policies.toString());

	    	try {
	    		policiesService.updatePolicies(policies);
	    	} catch (Exception e ) {
	    		result.append("\nException in putEmployer ...\n" + stackTrace(e));
	    		e.printStackTrace();
	    	}

	    	String newResult = policies.getFileNumber() + " policy details saved.";
	    	result.setLength(0);
	    	result.append(newResult);
		} catch ( Exception e ) {
			e.printStackTrace();
			result.append("\nException ... \n" + stackTrace(e));
		}
		return result.toString();
	}



	public ModelAndView updateDatabase() {
		ModelAndView model = new ModelAndView();
		int result = 0;
		int errors = 0;
		try {
			List<Employer> emplCache = EmployerController.getAndClearCache();

			logString("UPDATE Thread:   Employer List - " + emplCache.size());
			for ( Employer emp : emplCache ) {
				try {
					employerService.updateEmployer(emp);
					result++;
				} catch (Exception e) {
					errors++;
					logString(emp.getFn() + " - " + stackTrace(e));
				}
			}

			model.addObject("result", result);
			model.addObject("errors", errors);
			model.setViewName("SaveRecords");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	public static String stackTrace(Exception e) {
		StackTraceElement[] trace = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		for ( StackTraceElement s : trace ) {
			if (s.toString().contains("synesis")) {
				sb.append(s.toString() + "\n");
			}
		}
		return sb.toString();
	}

	
	public static void logString(String s) {
		try {
			String logFileName = Folders.BASE_DIRECTORY + "wc-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".log";
			PrintWriter logFile = new PrintWriter(new FileOutputStream(new File(logFileName),true));
			logFile.println(s);
			logFile.flush();
			logFile.close();
//			System.out.println(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
