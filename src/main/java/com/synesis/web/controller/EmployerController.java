package com.synesis.web.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.synesis.model.Employer;

public class EmployerController {

//	private static AtomicInteger arrayIndex = new AtomicInteger(0);

	private static List<Employer> employerGetCache = Collections.synchronizedList(new ArrayList<Employer>());
	private static List<Employer> employerPostCache = Collections.synchronizedList(new ArrayList<Employer>());

	
	public static void fillCache(List<Employer> employers) {
		employerGetCache.addAll(employers);
	}

	public static List<Employer> getPendingCache() {
		List<Employer> resultCache = Collections.synchronizedList(new ArrayList<Employer>(employerGetCache));
		return resultCache;
	}
	
	public static int remainingCache() {
//2018-12-22		return employerGetCache.size()-arrayIndex.get();
		return employerGetCache.size();
	}
	
//2018-12-22	public static int getArrayIndex() {
//		return arrayIndex.get();
//	}
	
	
	public static void putEmployer(Employer e) {
    	e.setUpdated(new Timestamp(System.currentTimeMillis()));
		employerPostCache.add(e);
	}

	public static Employer getEmployer() {
//2018-12-22		Employer e = employerGetCache.get(arrayIndex.getAndIncrement());
		Employer e = employerGetCache.remove(0);
		return e;
	}


	public static List<Employer> getEmployerCache() {
		List<Employer> resultCache = Collections.synchronizedList(new ArrayList<Employer>(employerPostCache));
		return resultCache;
	}

	public static List<Employer> getAndClearCache() {
		List<Employer> resultCache = Collections.synchronizedList(new ArrayList<Employer>(employerPostCache));
		// Potential to lose a few due to thread interruption, but they will be picked up again on the next cache fill.
		employerPostCache.clear();
		return resultCache;
	}

	public static int getCacheSize() {
		return employerPostCache.size();
	}

}
