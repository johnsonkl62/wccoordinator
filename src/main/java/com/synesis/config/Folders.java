package com.synesis.config;

public class Folders {

	//  Unix/Linux paths
	public static final String UNIX_BASE_DIRECTORY = "/synesis/";
    public static final String UNIX_COLLECTION_DIRECTORY = "/synesis/collection/";

    //  Windows paths
	public static final String WINDOWS_BASE_DIRECTORY = "D:\\Synesis\\";
    public static final String WINDOWS_COLLECTION_DIRECTORY = "D:\\Synesis\\Collection\\";

    //  Operational paths - default to Unix
	public static String BASE_DIRECTORY = UNIX_BASE_DIRECTORY;
    public static String COLLECTION_DIRECTORY = UNIX_COLLECTION_DIRECTORY;
    public static void setWindows() {
    	BASE_DIRECTORY = WINDOWS_BASE_DIRECTORY;
        COLLECTION_DIRECTORY = WINDOWS_COLLECTION_DIRECTORY;
    }
}
