package com.synesis.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.model.Employer;
import com.synesis.service.employers.EmployerService;
import com.synesis.web.controller.EmployerController;

@Component
public class DatabaseThread {

	@Autowired
	private EmployerService employerService;

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd\tHH:mm:ss");


	private static int lastSize = 0;

	// Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 */12 * * ?") // Every 10 minutes
	public void checkDatabase() {
		LocalDateTime ldt = LocalDateTime.now(); 

		List<Employer> emps = EmployerController.getEmployerCache();
		int eSize = emps.size();
		boolean increased = eSize > lastSize;

		if ( !increased ) {
			int hour = ldt.getHour();
			int minute = ldt.getMinute();
			boolean workHours = ((hour >= 7) && (hour <= 19));
			boolean offHours = ((hour == 6) || ((hour > 19) && (hour <= 23)));
			boolean sendEmail = false;
			
			String day = ldt.getDayOfWeek().name();
			
			if ("SATURDAY".equals(day) || "SUNDAY".equals(day)) {
				if ( workHours && minute==0 ) {
					sendEmail = true;
				}
				
			} else if ( Holidays.isHoliday(ldt) ) {
				if ( workHours && minute==0 && (hour/3 == 0) ) {
					sendEmail = true;
				}
			} else {
				if ( workHours && eSize == 0 ) {
					sendEmail = true;
				} else if ( offHours && (minute==0) ) {
					sendEmail = true;
				}
			}
			
			if ( sendEmail ) {
				SendMailUtility.emailReport("", "**WARNING** Employer Cache at " + ldt.format(dtf) + "  Current: " + eSize + "   Last: " + lastSize);
			}
		}
		lastSize = eSize;
	}


	// Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 5 */6 * * ?") // Every 6 hours
	public void updateDatabase() {
		try {
			StringBuffer sb = new StringBuffer("Employer retrieval report\r\n");
			sb.append("From IPv4: " + NetworkUtilities.getIPAddress(false) + "\r\n\r\n");

			List<Employer> emplCache = EmployerController.getAndClearCache();

			for ( Employer emp : emplCache ) {
				try {
					sb.append(emp.toString()+"\r\n");
					employerService.updateEmployer(emp);
				} catch (Exception e) {
					sb.append("ERROR OCCURRED:  " + emp.toFixedString()+"\r\n");
				}
			}

			LocalDateTime ldt = LocalDateTime.now();
			SendMailUtility.emailReport(sb.toString(), "Employer Cache at " + ldt.format(dtf) + " was " + emplCache.size());

			if ( EmployerController.remainingCache() < 2500 ) {
				EmployerController.fillCache(employerService.getEmployers());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static String stackTrace(Exception e) {
		StackTraceElement[] trace = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		for ( StackTraceElement s : trace ) {
			if (s.toString().contains("synesis")) {
				sb.append(s.toString() + "\n");
			}
		}
		return sb.toString();
	}

	private static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

}
