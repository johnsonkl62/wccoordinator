package com.synesis.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.config.Folders;
import com.synesis.service.employers.EmployerService;
import com.synesis.service.mysql.MysqlService;

@Component
public class StatusThread {

	@Autowired
	private EmployerService employerService;

	@Autowired
	private MysqlService mysqlService;

	private CollectionStatus[] stats = new CollectionStatus[5];  // Keep the last five days

	private int statIndex = 0;
	
	private HashMap<String, String> lastStatus = null;

	
	// Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 0 * * ?") // Every midnight
	public void updateCounts() {
		try {
			StringBuffer sb = new StringBuffer("Employer retrieval status report\r\n");
			try {
				stats[statIndex] = employerService.getCurrentStatus();
//				CollectionHistory.getInstance().addCollection(employerService.getCurrentStatus());

				for (int i = 0; i < stats.length; i++) {
					int j = ((statIndex + i) % stats.length);
					if (stats[j] != null) {
						sb.append(stats[j].toString());
					}
				}
				statIndex = (++statIndex % stats.length);

//				sb.append(CollectionHistory.getInstance().getTable(5));
				
			} catch (Exception e) {
				log(stackTrace(e));
				sb.append("ERROR OCCURRED:  " + stackTrace(e) + "\r\n");
			}

			try {
				sb.append("\r\n\r\n\r\nDatabase Status:\r\n");
				HashMap<String, String> variables = mysqlService.getStatus();
				for ( String key : variables.keySet() ) {
					if (lastStatus == null || !(variables.get(key).equalsIgnoreCase(lastStatus.get(key)))) {
						sb.append(key + "\t" + variables.get(key) + "\t" + (lastStatus != null ? lastStatus.get(key): "") + "\r\n");
					}
				}
				lastStatus = variables;
				
			} catch (Exception e) {
				log(stackTrace(e));
				sb.append("ERROR OCCURRED:  " + stackTrace(e) + "\r\n");
			}

			SendMailUtility.emailReport(sb.toString(), "Employer Collection Status");

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
//	select month(xdate),count(*) from employers where updated is null group by month(xdate) order by month(xdate);
//	+--------------+----------+
//	| month(xdate) | count(*) |
//	+--------------+----------+
//	|            1 |      793 |
//	|            2 |      386 |
//	|            3 |      427 |
//	|            4 |      467 |
//	|            5 |      447 |
//	|            6 |      454 |
//	|            7 |      614 |
//	|            8 |      559 |
//	|            9 |      650 |
//	|           10 |      858 |
//	|           11 |      618 |
//	|           12 |      345 |
//	+--------------+----------+

	
//  select month(updated) as m, year(updated) as y, count(*) from employers where updated is not null group by y,m order by y,m;
//	+------+------+----------+
//	| m    | y    | count(*) |
//	+------+------+----------+
//	|    2 | 2018 |        2 |
//	|    3 | 2018 |       13 |
//	|    4 | 2018 |        9 |
//	|    5 | 2018 |       10 |
//	|    6 | 2018 |       14 |
//	|    7 | 2018 |        9 |
//	|    8 | 2018 |       15 |
//	|    9 | 2018 |       58 |
//	|   10 | 2018 |      647 |
//	|   11 | 2018 |     6180 |
//	|   12 | 2018 |    16444 |
//	|    1 | 2019 |    21897 |
//	|    2 | 2019 |    22617 |
//	|    3 | 2019 |    13055 |
//	|    7 | 2019 |    10439 |
//	|    8 | 2019 |    43178 |
//	|    9 | 2019 |    30182 |
//	|   10 | 2019 |    20071 |
//	|   11 | 2019 |    34268 |
//	|   12 | 2019 |    22354 |
//	+------+------+----------+
//	20 rows in set (1.60 sec)


	private void log(String s) {
		PrintWriter pw = null;
		try {
			System.out.println(s);
			String logFileName = Folders.BASE_DIRECTORY + "ccUpdate-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ".log";
			pw = new PrintWriter(new FileOutputStream(new File(logFileName), true));
			pw.println(LocalDateTime.now().format(fdtf) + "\t" + s);
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}

	public static String stackTrace(Exception e) {
		StackTraceElement[] trace = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		for ( StackTraceElement s : trace ) {
			if (s.toString().contains("synesis")) {
				sb.append(s.toString() + "\n");
			}
		}
		return sb.toString();
	}

	private static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

}
