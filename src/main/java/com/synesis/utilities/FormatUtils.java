package com.synesis.utilities;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class FormatUtils {

	public static String fixedWidth(String s, int w) {
		try {
			String result = "";
			if (s == null) {
				while (result.length() < w) {
					result += " ";
				}
			
			} else if (s.length() > w) {
				result = s.substring(0, w - 3) + "...";

			} else if (s.length() == w) {
				result = s;
			
			} else {
				result = s;
				while (result.length() < w) {
					result += " ";
				}
			}
			return result;
		} catch (Exception e) {
			return s;
		}
	}

	public static DateTimeFormatter ddtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static DateTimeFormatter dtdtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat dsdf = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat dtsdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

}
