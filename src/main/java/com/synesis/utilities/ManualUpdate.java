package com.synesis.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class ManualUpdate {

	private static Connection mainConnection;
	
	static {
		try {
			System.out.println("Database initialization");

			try {
				Class.forName("com.mysql.jdbc.Driver");
				System.out.println("MySQL driver loaded");
			} catch (ClassNotFoundException cnfe) {
				throw new IllegalStateException("Cannot find JDBC driver");
			}
		} catch ( Exception e ) {
			
		}
	}


    public static Connection getConnection() {
    	try {
	    	if ( mainConnection == null || mainConnection.isClosed() ) {
				// Connect to the database
				String url = "jdbc:mysql://localhost:3306/wc?autoReconnect=true&useSSL=false";
				String username = "java";
				String password = "Java#WC#01";
	
				mainConnection = DriverManager.getConnection(url, username, password);
	    	}
    	} catch ( Exception e ) {
			e.printStackTrace();
//			e.printStackTrace(Server.logFile);
    		mainConnection = null;
    	}
    	return mainConnection;
    }
	
	
    private static String apos(String s) {
    	return "'" + s.replaceAll("'", "''") + "'";
    }
    
    private static String monthOf(String s) {
    	if ( s.equalsIgnoreCase("JAN") ) return "01";
    	if ( s.equalsIgnoreCase("FEB") ) return "02";
    	if ( s.equalsIgnoreCase("MAR") ) return "03";
    	if ( s.equalsIgnoreCase("APR") ) return "04";
    	if ( s.equalsIgnoreCase("MAY") ) return "05";
    	if ( s.equalsIgnoreCase("JUN") ) return "06";
    	if ( s.equalsIgnoreCase("JUL") ) return "07";
    	if ( s.equalsIgnoreCase("AUG") ) return "08";
    	if ( s.equalsIgnoreCase("SEP") ) return "09";
    	if ( s.equalsIgnoreCase("OCT") ) return "10";
    	if ( s.equalsIgnoreCase("NOV") ) return "11";
    	if ( s.equalsIgnoreCase("DEC") ) return "12";
    	return "";
    }
	
	public static void main(String[] args) {
		getConnection();
		try {
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			String line;
			
			while (  (line = br.readLine()) != null ) {
				if ( line.contains("\tPOST\t") ) {
				String[] fields = line.split("\t");
				
				String logDate = fields[0].trim();
				String logTime = fields[1].trim();
				String method = fields[2].trim();
//				String ip = fields[3].trim();

				String fn = fields[4].trim();
				String county = fields[5].trim();
				String updated = fields[6].trim();
				String carrier = fields[8].trim();
				String name = fields[9].trim();
				String addr = fields[10].trim();
				String city = fields[11].trim();
				String state = fields[12].trim();
				String zip = fields[13].trim();
				String xdateStr = fields[14].trim();    //  format:   Sun Mar 26 00:00:00 EDT 2017
				//                                                    0123456789012345678901234567
				String code = fields[15].trim();
				String mm = fields[16].trim();
				String delay = fields[17].trim();
				String secs = fields[18].trim();

				String xdate = xdateStr.substring(24, 28) + "-" + monthOf(xdateStr.substring(4,7)) + "-" + xdateStr.substring(8,10);
				
				try {
					String updateText = "UPDATE employers SET " + 
							"carrier=" + apos(carrier) + "," + 
							"updated=" + apos(updated) + "," +
//							"ip=" + apos(ip2) + "," +
							"name=" + apos(name) + "," +
							"addr=" + apos(addr) + "," +
							"city=" + apos(city) + "," +
							"state=" + apos(state) + "," +
							"zip=" + apos(zip) + "," +
							"xdate=" + apos(xdate) + "," +
							"code=" + apos(code) + "," +
							"mm=" + apos(mm) + " WHERE fn=" + apos(fn);
					
					System.out.println(updateText+";");

					Statement stmt = mainConnection.createStatement();
					int count = stmt.executeUpdate(updateText);
					if ( count < 1 ) {
						System.out.println("ERROR");
					}

				} catch ( Exception e ) {
					e.printStackTrace();
				}
			}
				}
			br.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

}
