package com.synesis.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.synesis.config.Folders;

public class GuassianDelays {

	private static Random r = new Random(LocalDateTime.now().getNano());

	private static DateTimeFormatter hsf = DateTimeFormatter.ofPattern("HH:mm:ss");
	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd\tHH:mm:ss");
	private static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");

	private static final int MINIMUM_HOLIDAY_REQUESTS = 12;
	private static final int MAXIMUM_HOLIDAY_REQUESTS = 50;
	
	private static final int MINIMUM_WEEKEND_REQUESTS = 200;
	private static final int MAXIMUM_WEEKEND_REQUESTS = 350;

	private static final int MINIMUM_DAILY_REQUESTS = 2100;
	private static final int MAXIMUM_DAILY_REQUESTS = 3000;

	private static final int meanPointSeconds = 46800;  //   1:00pm 
	private static final int stdDevSeconds = 14400;     //   9:00am - 5:0pm 
	
	private ArrayList<LocalTime> executionTimes = new ArrayList<LocalTime>();

	private static LocalDate resetDate = LocalDate.now().minusDays(1); 

	private static GuassianDelays singleton = new GuassianDelays();


	private GuassianDelays() {
	}

	public static GuassianDelays getInstance() {
		LocalDate today = LocalDate.now();
		if ( !today.isEqual(resetDate) ) {
			singleton.reset();
		}
		return singleton;
	}
	
	public String reset() {
//		System.out.println("Reset");
		executionTimes.clear();
		
		resetDate = LocalDate.now();

		//  Calculate a new request limit for today, based on weekday, weekend, or holiday
		int minDailyRequests = MINIMUM_DAILY_REQUESTS;
		int maxDailyRequests = MAXIMUM_DAILY_REQUESTS;
		
		//  If proposed start time is Saturday
		if ("SATURDAY".equals(resetDate.getDayOfWeek().name()) || "SUNDAY".equals(resetDate.getDayOfWeek().name())) {
			minDailyRequests = MINIMUM_WEEKEND_REQUESTS;
			maxDailyRequests = MAXIMUM_WEEKEND_REQUESTS;
		} else if ( Holidays.isHolidayDate(resetDate) ) {
			minDailyRequests = MINIMUM_HOLIDAY_REQUESTS;
			maxDailyRequests = MAXIMUM_HOLIDAY_REQUESTS;
		}
		
		int todaysDailyRequests = minDailyRequests + r.nextInt(maxDailyRequests - minDailyRequests);

		LocalTime lt = LocalTime.of(0, 0);
		for ( int i = 0; i < todaysDailyRequests; i++ ) {
			int delaySecond = -1;
			while (delaySecond < 0 || delaySecond >= 86400) {
				//  This should result in a negative value only in extremely rare cases.
				delaySecond = (int)((double)meanPointSeconds + (r.nextGaussian() * (double)stdDevSeconds));
			}
			executionTimes.add(lt.plusSeconds(delaySecond));
		}
		Collections.sort(executionTimes);
		
		try {
			PrintWriter pw = new PrintWriter(new File(Folders.BASE_DIRECTORY + "delays-" + LocalDateTime.now().format(fdtf) + ".txt"));
			for ( int i=0; i<executionTimes.size(); i++ ) {
				pw.println("" + i + "\t" + executionTimes.get(i).format(hsf));
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return executionTimes.stream().map(n -> n.toString()).collect(Collectors.joining("<br/>"));
	}


	public int getDelay() {
		LocalTime rqt = LocalTime.now();
		LocalDate ld = LocalDate.now();
		
		if ( !ld.isEqual(resetDate) ) {
			reset();
		}

		try {
			for (int i = 0; i < executionTimes.size(); i++) {
				LocalTime cpt = executionTimes.get(i);
				if ( cpt.isAfter(rqt) ) {
					if ( i == 0 ) {
//						System.out.println("\t\tRequest: " + rqt.toSecondOfDay() + "\t(-1) 0\t(" + i + ") " + cpt.toSecondOfDay() + "\t" + (cpt.toSecondOfDay() - rqt.toSecondOfDay()));
						return (cpt.toSecondOfDay() - rqt.toSecondOfDay());
					} else if (cpt.equals(rqt)) {
						LocalTime pt = executionTimes.get(i-1);
//						System.out.println("\t\tRequest: " + rqt.toSecondOfDay() + "\t(" + (i-1) + ") "  + pt.toSecondOfDay() + "\t(" + i + ") " + cpt.toSecondOfDay() + "\t" + (cpt.toSecondOfDay() - rqt.toSecondOfDay()));
						return 0;
					} else {
						LocalTime pt = executionTimes.get(i-1);
						if ( pt.isBefore(rqt) ) {
//							System.out.println("\t\tRequest: " + rqt.toSecondOfDay() + "\t(" + (i-1) + ") "  + pt.toSecondOfDay() + "\t(" + i + ") " + cpt.toSecondOfDay() + "\t" + (cpt.toSecondOfDay() - rqt.toSecondOfDay()));
							return (cpt.toSecondOfDay() - rqt.toSecondOfDay());
						}
					}
				}
			}
			//  Use first past midnight
			System.out.println("\t\tRequest: " + rqt.toSecondOfDay() + "\t(" + executionTimes.size() + ") 86400\t" + executionTimes.get(0).toSecondOfDay() + "\t" + ((86400 - rqt.toSecondOfDay()) + executionTimes.get(0).toSecondOfDay()));
			return (86400 - rqt.toSecondOfDay()) + executionTimes.get(0).toSecondOfDay();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return 600 + r.nextInt(3000);
	}


//	public static void main(String[] args) {
//		Folders.setWindows();
//		
//		GuassianDelays dc = GuassianDelays.getInstance();
//
//		LocalTime[] times = new LocalTime[24];
//		for ( int i = 0; i < times.length; i++ ) {
//			times[i] = LocalTime.of(i, 0);
//			try {
////				dc.getDelay(times[i]);
////				System.out.println("Test case " + i + ": ");
////				System.out.println(times[i].format(hsf) + "\t" + dc.getDelay(times[i]));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//

	//	}


//	private static void log(String s) {
//		try {
//			LocalDateTime ldt = LocalDateTime.now();
//			PrintWriter pw = new PrintWriter(new FileOutputStream(new File(Folders.BASE_DIRECTORY + "GC-" + ldt.format(ddtf) + ".log")),true);
//			pw.println(s);
//			pw.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//	}

}