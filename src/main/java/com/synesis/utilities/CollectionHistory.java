package com.synesis.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Queue;

import org.apache.commons.collections4.queue.CircularFifoQueue;

public class CollectionHistory {

	Queue<CollectionStatus> history = new CircularFifoQueue<CollectionStatus>(30);

	
	private static CollectionHistory collectionHistory = new CollectionHistory(); 
	
	private CollectionHistory() {
	}
	
	public static CollectionHistory getInstance() {
		return collectionHistory;
	}

	public void addCollection(CollectionStatus stats) {
		history.add(stats);
	}

	public StringBuffer getTable(int days) {
		StringBuffer sb = new StringBuffer();

		//  Find oldest month in all stats
		LocalDate ld = LocalDate.now();
		ld.minusDays(ld.getDayOfMonth()-1);

		Iterator<CollectionStatus> it = history.iterator();
		while ( it.hasNext() ) {
			CollectionStatus cs = it.next();
			LocalDateTime chk = cs.getStatusDate();

		}

		return sb;
	}
}
