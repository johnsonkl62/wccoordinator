package com.synesis.utilities;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Holidays {

	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	static private ArrayList<LocalDate> holidayList = new ArrayList<LocalDate>();

	static {
		holidayList.add(LocalDate.parse("2021-01-01", dtf));  //  New Year's Day
		holidayList.add(LocalDate.parse("2021-05-31", dtf));  //  Memorial Day
		holidayList.add(LocalDate.parse("2021-07-05", dtf));  //  Independence Day
		holidayList.add(LocalDate.parse("2021-09-06", dtf));  //  Labor Day
		holidayList.add(LocalDate.parse("2021-11-25", dtf));  //  Thanksgiving
		holidayList.add(LocalDate.parse("2021-11-26", dtf));  //  Black Friday
		holidayList.add(LocalDate.parse("2021-12-23", dtf));  //  Christmas Eve
		holidayList.add(LocalDate.parse("2021-12-24", dtf));  //  Christmas
		holidayList.add(LocalDate.parse("2021-12-31", dtf));  //  New Year's Eve

		holidayList.add(LocalDate.parse("2022-05-30", dtf));  //  Memorial Day
		holidayList.add(LocalDate.parse("2022-07-04", dtf));  //  Independence Day
		holidayList.add(LocalDate.parse("2022-09-05", dtf));  //  Labor Day
		holidayList.add(LocalDate.parse("2022-11-24", dtf));  //  Thanksgiving
		holidayList.add(LocalDate.parse("2022-11-25", dtf));  //  Black Friday
		holidayList.add(LocalDate.parse("2022-12-23", dtf));  //  Christmas Eve
		holidayList.add(LocalDate.parse("2022-12-26", dtf));  //  Christmas Day

		holidayList.add(LocalDate.parse("2023-01-01", dtf));  //  New Year's Day
		holidayList.add(LocalDate.parse("2023-05-29", dtf));  //  Memorial Day
		holidayList.add(LocalDate.parse("2023-07-04", dtf));  //  Independence Day
		holidayList.add(LocalDate.parse("2023-09-04", dtf));  //  Labor Day
		holidayList.add(LocalDate.parse("2023-11-23", dtf));  //  Thanksgiving
		holidayList.add(LocalDate.parse("2023-11-24", dtf));  //  Black Friday
		holidayList.add(LocalDate.parse("2023-12-22", dtf));  //  Christmas
		holidayList.add(LocalDate.parse("2023-12-25", dtf));  //  Christmas
		holidayList.add(LocalDate.parse("2023-12-29", dtf));  //  New Year's Eve

		holidayList.add(LocalDate.parse("2024-01-01", dtf));  //  New Year's Day
		holidayList.add(LocalDate.parse("2024-05-27", dtf));  //  Memorial Day
		holidayList.add(LocalDate.parse("2024-07-04", dtf));  //  Independence Day
		holidayList.add(LocalDate.parse("2024-09-02", dtf));  //  Labor Day
		holidayList.add(LocalDate.parse("2024-11-28", dtf));  //  Thanksgiving
		holidayList.add(LocalDate.parse("2024-11-29", dtf));  //  Black Friday
		holidayList.add(LocalDate.parse("2024-12-24", dtf));  //  Christmas Eve
		holidayList.add(LocalDate.parse("2024-12-25", dtf));  //  Christmas
		holidayList.add(LocalDate.parse("2024-12-26", dtf));  //  Christmas
		holidayList.add(LocalDate.parse("2024-12-31", dtf));  //  New Year's Eve
	}
	
	public static boolean isHoliday(LocalDateTime ldt) {
		LocalDate ld = ldt.toLocalDate();
		for ( LocalDate h : holidayList ) {
			if ( ld.isEqual(h) ) {
				return true;
			} else if ( ld.isBefore(h) ) {
				break;
			}
		}
		return false;
	}

	public static boolean isHolidayDate(LocalDate ld) {
		for ( LocalDate h : holidayList ) {
			if ( ld.isEqual(h) ) {
				return true;
			} else if ( ld.isBefore(h) ) {
				break;
			}
		}
		return false;
	}
	public static void main(String[] args) {
		Holidays hs = new Holidays();
		LocalDateTime ld = LocalDateTime.now();
		System.out.println(ld + " is " + (Holidays.isHoliday(ld) ? "" : " NOT ") + " a holiday." );
	}

}
