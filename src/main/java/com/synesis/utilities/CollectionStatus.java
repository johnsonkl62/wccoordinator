package com.synesis.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class CollectionStatus {

	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private LocalDateTime ldt = LocalDateTime.now();

	private ArrayList<CollectionStat> updatedCounts = new ArrayList<CollectionStat>();
	private ArrayList<CollectionStat> nullCounts = new ArrayList<CollectionStat>();


	public void addUpdatedCount( int year, int month, int count ) {
		updatedCounts.add(new CollectionStat(year, month, count));
	}
	
	public void addNullCount( int month, int count ) {
		nullCounts.add(new CollectionStat(month, ldt.getYear(), count));
	}

	public LocalDateTime getStatusDate() {
		return ldt;
	}

	public ArrayList<CollectionStat> getUpdatedCounts() {
		return updatedCounts;
	}

	public ArrayList<CollectionStat> getNullCounts() {
		return nullCounts;
	}


	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("As of " + dtf.format(ldt) + "\r\n");

		//  nulls
		sb.append("\r\nNull Counts (where updated is null)\r\n\r\n");
		for ( CollectionStat cs : nullCounts ) {
			sb.append(cs.toString() + "\r\n");
		}

		//  updates
		sb.append("\r\n\r\n\r\nUpdated Counts (where updated is not null)\r\n\r\n");
		for ( CollectionStat cs : updatedCounts ) {
			sb.append(cs.toString() + "\r\n");
		}
		sb.append("\r\n\r\n");
		return sb.toString();
	}
	
	
}
