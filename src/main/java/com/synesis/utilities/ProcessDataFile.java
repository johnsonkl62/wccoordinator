package com.synesis.utilities;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.tidy.Tidy;

public class ProcessDataFile implements FileVisitor<Path> {

	static private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	String updateDate = "0000-00-00";
	
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		updateDate = dir.getFileName().toString();
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

		try {
			String fileName = file.getFileName().toString();
//			System.out.println("Processing file:  " + fileName);
			String fileNumber = fileName.substring(fileName.indexOf(PREFIX) + PREFIX.length(), fileName.indexOf("."));
			System.out.println("Processing file number:  " + fileNumber);

			String contents = new String(Files.readAllBytes(file)).trim();
			if ( contents != null && !"null".equals(contents) ) {
				Document doc = getDocument(new ByteArrayInputStream(contents.getBytes()));
	
				String infoFrag = getInnerHTML((Node)xpathSearch(doc, "//div[@class='content']", XPathConstants.NODE));;
				String dataFrag = getInnerHTML((Node)xpathSearch(doc, "//table[@style='padding-left: 15px']", XPathConstants.NODE));;
				
				infoFrag = infoFrag.replaceAll("<h1>Policy Coverage</h1>", "<h1>Policy Coverage (as of " + updateDate + ")</h1>");
				
//				System.out.println(infoFrag);
				
				try {
//					PrintWriter pw = new PrintWriter(Folders.BASE_DIRECTORY + "Test-Output-" + fileNumber + ".html");
//					pw.print("<html><body>");
//					pw.print(infoFrag);
//					pw.print("<br/><br/>");
//					pw.print(dataFrag);
//					pw.print("</body></html>");
//					pw.flush();
//					pw.close();
					updateDatabase(fileNumber, infoFrag, dataFrag);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}		
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		System.out.println("visit file failed: " + file);
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		System.out.println("post visit directory: " + dir);
		return FileVisitResult.CONTINUE;
	}


	private static final String PREFIX = "pcrb-details-"; 
	

    public static Object xpathSearch(Node tidyDOM, String expression, QName type){
        XPath xPath = XPathFactory.newInstance().newXPath();
        try {
            XPathExpression xPathExpression = xPath.compile(expression);
            Object o = xPathExpression.evaluate(tidyDOM, type); 
            return o;
        } catch (Exception e) {
            return null;
        }
    }

    public static Document getDocument(InputStream response){
        Tidy tidy = new Tidy();
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        return tidy.parseDOM(response, null);
    }


//	public static String getHtmlFragment(String html) {
//        if (html != null ) {
//	        try {
//				Document doc = getDocument(new ByteArrayInputStream(html.getBytes()));
//	
//				Node content = ;
//				
//				return 
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//	    }
//    	return null;
// 	}
	
	public static String getInnerHTML(Node node) throws TransformerConfigurationException, TransformerException
	{
	    StringWriter sw = new StringWriter();
	    Result result = new StreamResult(sw);
	    TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
	    Transformer proc = factory.newTransformer();
	    proc.setOutputProperty(OutputKeys.METHOD, "html");
	    for (int i = 0; i < node.getChildNodes().getLength(); i++)
	    {
	        proc.transform(new DOMSource(node.getChildNodes().item(i)), result);
	    }
	    return sw.toString();
	}


	private void updateDatabase(String fileNumber, String infoFrag, String dataFrag) {
		try {
			Connection c = getConnection();
			try {
				insertStatement.setString(1, fileNumber);
				insertStatement.setString(2, "" + infoFrag + "<br/>" + dataFrag + "");
				insertStatement.executeUpdate();
			} catch ( SQLException se ) {
				updateStatement.setString(1, fileNumber);
				updateStatement.setString(2, "" + infoFrag + "<br/>" + dataFrag + "");
				updateStatement.executeUpdate();
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	
	private static Connection mainConnection;
	private static PreparedStatement insertStatement;
	private static PreparedStatement updateStatement;
	
	
	static {
		try {
			System.out.println("Database initialization");

			try {
				Class.forName("com.mysql.jdbc.Driver");
				System.out.println("MySQL driver loaded");
			} catch (ClassNotFoundException cnfe) {
				throw new IllegalStateException("Cannot find JDBC driver");
			}
		} catch ( Exception e ) {
			
		}
	}


    public static Connection getConnection() {
    	try {
	    	if ( mainConnection == null || mainConnection.isClosed() ) {
				// Connect to the database
				String url = "jdbc:mysql://localhost:3306/wc?autoReconnect=true&useSSL=false";
				String username = "java";
				String password = "Java#WC#01";

				mainConnection = DriverManager.getConnection(url, username, password);
	    	}
    	} catch ( Exception e ) {
			e.printStackTrace();
//			e.printStackTrace(Server.logFile);
    		mainConnection = null;
    	}
    	return mainConnection;
    }
	
	
}
