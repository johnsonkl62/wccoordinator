package com.synesis.utilities;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

public class NetworkUtilities {

	public static String getIPAddress(boolean v6) throws SocketException {
	    Enumeration<NetworkInterface> netInts = NetworkInterface.getNetworkInterfaces();
	    for (NetworkInterface netInt : Collections.list(netInts)) {
	        if (netInt.isUp() && !netInt.isLoopback()) {
	            for (InetAddress inetAddress : Collections.list(netInt.getInetAddresses())) {

	                if (inetAddress.isLoopbackAddress()
	                     || inetAddress.isLinkLocalAddress()
	                     || inetAddress.isMulticastAddress()) {
	                    continue;
	                }

	                if (v6 && inetAddress instanceof Inet6Address) {
	                    return inetAddress.getHostAddress();
	                }

	                if (!v6 && inetAddress instanceof InetAddress) {
	                    return inetAddress.getHostAddress();
	                }

	            }
	        }
	    }
	    return null;
	}
}
