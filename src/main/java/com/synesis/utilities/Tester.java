package com.synesis.utilities;

public class Tester {

	private String code = "";
	
	public void setCode(String code) {
		this.code = code;
		if ( code == null || !isAlphaNumeric(code) ) { 
			this.code = "";
		}
		// Remove leading zeroes
		try {
			Integer i = Integer.parseInt(this.code);
			this.code = i.toString();
		} catch (Exception e) {
		}
	}

	private boolean isAlphaNumeric(String code) {
		return code.matches("[a-zA-Z0-9]+");
	}

	public static void main(String[] args) {
		Tester t = new Tester();
		t.setCode("0005");
	}

}
