package com.synesis.utilities;

public class CollectionStat {

	private Integer year = null;
	private Integer month = null;
	private Long count = (long)-1;

	public CollectionStat(int mn, int yr, long ct) {
		year = yr;
		month = mn;
		count = ct;
	}

	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Year: " + year + "\tMonth: " + month + "\tCount: " + count;
	}
}
