package com.synesis.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.synesis.config.Folders;

public class GuassianCalculator {

	private static Random r = new Random(LocalDateTime.now().getNano());

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd\tHH:mm:ss");
	private static DateTimeFormatter fdtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
//	private static DateTimeFormatter ddtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static final int MAXIMUM_HOLIDAY_REQUESTS = 50;
	private static final int MINIMUM_HOLIDAY_REQUESTS = 12;
	
	private static final int MAXIMUM_WEEKEND_REQUESTS = 350;
	private static final int MINIMUM_WEEKEND_REQUESTS = 200;

	private static final int MAXIMUM_DAILY_REQUESTS = 3000;
	private static final int MINIMUM_DAILY_REQUESTS = 2100;

	private static final int meanPointSeconds = 46800;  //   1:00pm 
	private static final int stdDevSeconds = 14400;     //   9:00am - 5:0pm 
	
	private ArrayList<Integer> delays = new ArrayList<Integer>();
	private AtomicInteger delayIndex = new AtomicInteger(-1);

	private int negativeCount = 0;

	private static LocalDateTime resetTime = LocalDateTime.now(); 
	
	private static GuassianCalculator singleton = new GuassianCalculator();
	
	private GuassianCalculator() {
	}

	public static GuassianCalculator getInstance() {
		if ( LocalDateTime.now().getDayOfMonth() != resetTime.getDayOfMonth() ) {
			resetTime = LocalDateTime.now();
			singleton.reset(resetTime);
		}
		return singleton;
	}
	
	
	
	public String reset(LocalDateTime requestTime) {
//		log("");
//		log("");
//		log("RESET");
//		log("");
//		log("");
		delays.clear();
		
		//  Calculate a new request limit for today, based on weekday, weekend, or holiday
		int minDailyRequests = MINIMUM_DAILY_REQUESTS;
		int maxDailyRequests = MAXIMUM_DAILY_REQUESTS;
		
		//  If proposed start time is Saturday
		if ("SATURDAY".equals(requestTime.getDayOfWeek().name()) || "SUNDAY".equals(requestTime.getDayOfWeek().name())) {
			minDailyRequests = MINIMUM_WEEKEND_REQUESTS;
			maxDailyRequests = MAXIMUM_WEEKEND_REQUESTS;
		} else if ( Holidays.isHoliday(requestTime) ) {
			minDailyRequests = MINIMUM_HOLIDAY_REQUESTS;
			maxDailyRequests = MAXIMUM_HOLIDAY_REQUESTS;
		}
		
//		log("Reset\t" + requestTime.format(dtf) + "\tMinimum Requests\t" + minDailyRequests);
//		log("Reset\t" + requestTime.format(dtf) + "\tMaximum Requests\t" + maxDailyRequests);

		int todaysDailyRequests = minDailyRequests + r.nextInt(maxDailyRequests - minDailyRequests);
//		log("Reset\t" + requestTime.format(dtf) + "\tToday's Requests\t" + todaysDailyRequests);

		for ( int i = 0; i < todaysDailyRequests; i++ ) {
			int delaySecond = -1;
			while (delaySecond < 0 || delaySecond >= 86400) {
				//  This should result in a negative value only in extremely rare cases.
				delaySecond = (int)((double)meanPointSeconds + (r.nextGaussian() * (double)stdDevSeconds));
			}
			delays.add(delaySecond);
		}
		Collections.sort(delays);
		
		try {
			PrintWriter pw = new PrintWriter(new File(Folders.BASE_DIRECTORY + "delays-" + requestTime.format(fdtf) + ".txt"));
			LocalDateTime ldt = LocalDateTime.now();
			for ( int i=0; i<delays.size(); i++ ) {
				ldt = ldt.withHour(0).withMinute(0).withSecond(0);
				ldt = ldt.plusSeconds(delays.get(i));
				pw.println("" + i + "\t" + delays.get(i) + "\t" + ldt.format(dtf));
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return delays.stream().map(n -> n.toString()).collect(Collectors.joining("<br/>"));
	}
	
	
	public int getDelay(LocalDateTime ct) {
		int result = -1;
		try {
			//  The first call for this object.  Must set the delayIndex based on the current time.
			int currentSecond = (ct.getHour()*3600) + (ct.getMinute()*60) + ct.getSecond();

			//  First call or midnight rollover.  "delays" array actually contains execution seconds throughout the day.
			if ( delays.size() == 0 ) {
//				log("getDelay\tdelay array is empty");
				reset(ct);
				delayIndex.set(-1);
			}

			//  End of day processing.  Calculate a new set of execution times.
			if ( delayIndex.get() >= delays.size() ) {
//				log("Delays.size(): " + delays.size() + "   Last time: " + delays.get(delays.size()-1));
				long secondsToMidnight = 0;

				LocalDateTime ldt = ct.withHour(0).withMinute(0).withSecond(0);
				if ( currentSecond > 80000 ) {
					ldt = ldt.plusDays(1);
					secondsToMidnight = Math.abs(ChronoUnit.SECONDS.between(ldt, ct)); 
				}

//				log("Seconds to Midnight: " + secondsToMidnight);
				reset(ldt);
				result = (int)secondsToMidnight + delays.get(0);
//				log("First execution time: " + delays.get(0));
				delayIndex.set(1);
//				log("Result: " + result);
				return result;
			}

			//  First call again?  delayIndex keeps track of where in the day we are.  If the next execution time has
			//  already passed, then the delay is zero.
			if ( delayIndex.get() != -1 ) {
				result = delays.get(delayIndex.getAndIncrement());
			} else {
//				log("getDelay\tdelayIndex is still -1");
				result = resetDelayIndex(result, currentSecond);
			}

			if ( result == -1 ) {
//				log("getDelay\tresult is -1");
				reset(ct);
				result = 86400 - currentSecond;
				result += delays.get(0);
			}
			result -= currentSecond;

			if ( result < -(3600*18) ) {
//				log("getDelay\tresult is less than 18 hours.  result=" + result);
				result += currentSecond;	
				result += (86400 - currentSecond);
				result += delays.get(delayIndex.get()-1);

			} else if ( result < 0 ) {
				negativeCount++;
				if ( negativeCount > 5 ) {
//					log("\r\n\r\n\r\ngetDelay\tNEGATIVE COUNT LIMIT REACHED.  Skipping retrievals.");
					negativeCount = 0;
					result = resetDelayIndex(result, currentSecond);
					result -= currentSecond;
				}
			}
			
		} catch ( Exception e ) {
			e.printStackTrace();
			result = 600 + r.nextInt(3000);
		}
		result = Math.max(result, 0);
		return result;
	}

	private int resetDelayIndex(int result, int currentSecond) {
		for ( int i = 0; i < delays.size(); i++ ) {
			if ( currentSecond < delays.get(i) ) {
				result = delays.get(i);
				delayIndex.set(i + 1);
				break;
			}
		}
		return result;
	}


	public static void main(String[] args) {

//		int grandTotal = 0;

		LocalDateTime lt = LocalDateTime.now();

		for (int k = 0; k < 1000; k++) {

		try {
			LocalDateTime et = lt.plusHours(24);
			LocalDateTime ct = lt;

			PrintWriter pw = new PrintWriter(new File("Sequence-" + lt.format(fdtf) + ".txt"));
			
			lt = lt.minusHours(1);

//			int requests = 0;
			GuassianCalculator dc = GuassianCalculator.getInstance();
			while (ct.isBefore(et)) {

				int randomDelay = dc.getDelay(ct);
				if ( randomDelay > 86400 ) {
//					log(ct.format(dtf) + "\t" + randomDelay + "\t");
					pw.print(ct.format(dtf) + "\t" + randomDelay + "\t");
//					log(ct.format(dtf));
//					pw.println(ct.format(dtf));
				}
//				requests++;
//				System.out.println(ct.format(dtf) + "\t" + randomDelay + "\t");
//				pw.print(ct.format(dtf) + "\t" + randomDelay + "\t");
				ct = ct.plusSeconds(randomDelay);
				//  Simulate call with random seconds between 4 and 10
				int execSeconds = 4 + r.nextInt(6);
				ct = ct.plusSeconds(execSeconds);
				
//				log(ct.format(dtf));
//				pw.println(ct.format(dtf));
			
			}
//			log("");
//			log("Total Requests: " + requests);

			pw.flush();
			pw.close();

			lt = et;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		}		
	}


//	private static void log(String s) {
//		try {
//			LocalDateTime ldt = LocalDateTime.now();
//			PrintWriter pw = new PrintWriter(new FileOutputStream(new File(Folders.BASE_DIRECTORY + "GC-" + ldt.format(ddtf) + ".log")),true);
//			pw.println(s);
//			pw.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//	}

}